package polymorphism;

public class BookStore {

    public static void main(String[] args) {
        Book[] books = new Book[5];
        books[0] = new Book("t0", "a0");
        books[1] = new ElectronicBook("t1", "a1", 2);
        books[2] = new Book("t2", "a2");
        books[3] = new ElectronicBook("t3", "a3", 2);
        books[4] = new ElectronicBook("t4", "a4", 2);
        for(Book i : books) {
            System.out.println(i);
        }
        ElectronicBook b = (ElectronicBook)books[1];
        System.out.println(b.getNumberBytes());

        ElectronicBook b2 = (ElectronicBook)books[0];
        System.out.println(b2.getNumberBytes());

    }
}
